import java.util.Set;

public abstract class Animal<Sp extends AnimalSpecies, This extends Animal<Sp, This>> extends Organism<Sp, This> {
    private double energyConsumedThisCycle = 0;

    protected void consumeEnergy(double energy) {
        this.energyConsumedThisCycle += energy;
    }

    public TickResult<This> tick() {
        var result = this.birthAndDeath(this.energyConsumedThisCycle);
        this.energyConsumedThisCycle = 0;
        return result;
    }

    protected Animal(Sp species, int age, double energie) {
        super(species, age, energie);
    }

    public double getDebrouillardise() {
        return this.getSpecies().getDebrouillardise();
    }

    public Set<String> getAliments() {
        return this.getSpecies().getAliments();
    }
}
