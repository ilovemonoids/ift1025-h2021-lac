public abstract class Species {
    private final String nomEspece;
    private final double besoinEnergie;
    private final double efficaciteEnergie;
    private final double resilience;
    private final double fertilite;
    private final int ageFertilite;
    private final double energieEnfant;
    private final double tailleMaximum;

    protected Species(
        String nomEspece,
        double besoinEnergie,
        double efficaciteEnergie,
        double resilience,
        double fertilite,
        int ageFertilite,
        double energieEnfant,
        double tailleMaximum)
    {
        this.nomEspece = nomEspece;
        this.besoinEnergie = besoinEnergie;
        this.efficaciteEnergie = efficaciteEnergie;
        this.resilience = resilience;
        this.fertilite = fertilite;
        this.ageFertilite = ageFertilite;
        this.energieEnfant = energieEnfant;
        this.tailleMaximum = tailleMaximum;
    }

    public String getNomEspece() {
        return this.nomEspece;
    }

    public double getBesoinEnergie() {
        return this.besoinEnergie;
    }

    public double getEfficaciteEnergie() {
        return this.efficaciteEnergie;
    }

    public double getResilience() {
        return this.resilience;
    }

    public double getFertilite() {
        return this.fertilite;
    }

    public int getAgeFertilite() {
        return this.ageFertilite;
    }

    public double getEnergieEnfant() {
        return this.energieEnfant;
    }

    public double getTailleMaximum() {
        return this.tailleMaximum;
    }
}

