public final class PlantSpecies extends Species {
    protected PlantSpecies(
        String nomEspece,
        double besoinEnergie,
        double efficaciteEnergie,
        double resilience,
        double fertilite,
        int ageFertilite,
        double energieEnfant,
        double tailleMaximum)
    {
        super(
            nomEspece,
            besoinEnergie,
            efficaciteEnergie,
            resilience,
            fertilite,
            ageFertilite,
            energieEnfant,
            tailleMaximum);
    }
}
