import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public final class Lac {
    private final int energieSolaire;
    private final List<Plante> plantes;
    private final List<Herbivore> herbivores;
    private final List<Carnivore> carnivores;

    public Lac(
        int energieSolaire, List<Plante> plantes, List<Herbivore> herbivores, List<Carnivore> carnivores)
    {
        this.energieSolaire = energieSolaire;
        this.plantes = plantes;
        this.herbivores = herbivores;
        this.carnivores = carnivores;
    }

    /**
     * Avance la simulation de un cycle.
     */
    public void tick() {
        double ePlantes = this.plantes.stream().mapToDouble(Plante::getEnergie).sum();
        this.plantes.stream()
            .map(plante -> plante.tick(this.energieSolaire * plante.getEnergie() / ePlantes))
            .collect(toUnmodifiableList())
            .forEach(result -> result.run(this.plantes));
        Collections.shuffle(this.plantes);

        this.herbivores.stream()
            .flatMap(herbivore -> herbivore.graze().stream())
            .forEach(result -> result.graze(this.plantes));
        this.herbivores.stream()
            .map(Herbivore::tick)
            .collect(toUnmodifiableList())
            .forEach(result -> result.run(this.herbivores));

        this.carnivores.stream()
            .flatMap(carnivore -> carnivore.hunt().stream())
            .forEach(result -> result.eat(this.herbivores));
        this.carnivores.stream()
            .map(Carnivore::tick)
            .collect(Collectors.toUnmodifiableList())
            .forEach(result -> result.run(this.carnivores));
    }

    public void imprimeRapport(PrintStream out) {
        this.imprimeRapportKind(out, this.plantes, "de plantes");
        this.imprimeRapportKind(out, this.herbivores, "d'herbivores");
        this.imprimeRapportKind(out, this.carnivores, "de carnivores");
    }

    private void imprimeRapportKind(PrintStream out, Collection<? extends Organism<?, ?>> organismes, String kind) {
        var statisticsBySpecies = organismes.stream()
            .collect(groupingBy(Organism::getNomEspece, summarizingDouble(Organism::getEnergie)));
        out.format("Il reste %s espèces %s.\n", statisticsBySpecies.size(), kind);
        for (var entry : statisticsBySpecies.entrySet()) {
            var value = entry.getValue();
            out.printf(
                "%s: %d individus qui contiennent en tout %.2f unités d'énergie.\n",
                entry.getKey(),
                value.getCount(),
                value.getSum());
        }
    }
}
