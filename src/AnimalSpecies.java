import java.util.Collection;
import java.util.Set;

public abstract class AnimalSpecies extends Species {
    private final double debrouillardise;
    private final Set<String> aliments;

    protected AnimalSpecies(
        String nomEspece,
        double besoinEnergie,
        double efficaciteEnergie,
        double resilience,
        double fertilite,
        int ageFertilite,
        double energieEnfant,
        double tailleMaximum,
        double debrouillardise,
        Collection<String> aliments)
    {
        super(
            nomEspece,
            besoinEnergie,
            efficaciteEnergie,
            resilience,
            fertilite,
            ageFertilite,
            energieEnfant,
            tailleMaximum);
        this.debrouillardise = debrouillardise;
        this.aliments = Set.copyOf(aliments);
    }

    public double getDebrouillardise() {
        return this.debrouillardise;
    }

    public Set<String> getAliments() {
        return this.aliments;
    }
}
