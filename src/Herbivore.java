import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static java.util.stream.Collectors.toUnmodifiableList;

public final class Herbivore extends Animal<HerbivoreSpecies, Herbivore> {
    public Herbivore(HerbivoreSpecies species, int age, double energie) {
        super(species, age, energie);
    }

    public Collection<GrazeResult> graze() {
        var grazeCount = 0;
        while (Math.random() < this.getDebrouillardise()) {
            grazeCount += 1;
        }

        var result = this.new GrazeResult();
        return Collections.nCopies(grazeCount, result);
    }

    public double getDebrouillardise() {
        return this.getSpecies().getDebrouillardise();
    }

    public double getVoraciteMin() {
        return this.getSpecies().getVoraciteMin();
    }

    public double getVoraciteMax() {
        return this.getSpecies().getVoraciteMax();
    }

    @Override
    protected Herbivore makeBaby() {
        return new Herbivore(this.getSpecies(), 0, this.getEnergieEnfant());
    }

    @Override
    protected Died<Herbivore> kill() {
        return new Died<>(this);
    }

    public final class GrazeResult {
        public void graze(List<Plante> plantes) {
            var candidates = plantes.stream()
                .filter(plante -> Herbivore.this.getAliments().contains(plante.getNomEspece()))
                .collect(toUnmodifiableList());

            if (candidates.isEmpty()) {
                return;
            }

            var eaten = candidates.get(ThreadLocalRandom.current().nextInt(candidates.size()));

            var voraciteMin = Herbivore.this.getVoraciteMin();
            var voracite = voraciteMin + Math.random() * (Herbivore.this.getVoraciteMax() - voraciteMin);
            var energyEaten = eaten.consume(voracite);
            Herbivore.this.consumeEnergy(energyEaten);
        }
    }
}
