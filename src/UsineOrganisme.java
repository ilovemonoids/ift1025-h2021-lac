public abstract class UsineOrganisme<Sp> {
    private String nomEspece;
    private Double besoinEnergie;
    private Double efficaciteEnergie;
    private Double resilience;
    private Double fertilite;
    private Integer ageFertilite;
    private Double energieEnfant;
    private Double tailleMaximum;
    private Sp species;

    protected final void clearSpecies() {
        this.species = null;
    }

    public void setNomEspece(String nomEspece) throws ConditionsInitialesInvalides {
        if (nomEspece == null || nomEspece.isBlank()) {
            throw new ConditionsInitialesInvalides("nomEspece ne doit pas être vide");
        }

        this.clearSpecies();
        this.nomEspece = nomEspece;
    }

    public void setBesoinEnergie(double besoinEnergie) throws ConditionsInitialesInvalides {
        if (besoinEnergie <= 0) {
            throw new ConditionsInitialesInvalides("besoinEnergie doit être supérieur à 0");
        }

        this.clearSpecies();
        this.besoinEnergie = besoinEnergie;
    }

    public void setEfficaciteEnergie(double efficaciteEnergie) throws ConditionsInitialesInvalides {
        if (0 > efficaciteEnergie || efficaciteEnergie > 1) {
            throw new ConditionsInitialesInvalides(
                "efficaciteEnergie doit être supérieur ou égal à 0 et inférieur ou égal à 1");
        }

        this.clearSpecies();
        this.efficaciteEnergie = efficaciteEnergie;
    }

    public void setResilience(double resilience) throws ConditionsInitialesInvalides {
        if (0 > resilience || resilience > 1) {
            throw new ConditionsInitialesInvalides(
                "resilience doit être supérieur ou égal à 0 et inférieur ou égal à 1");
        }

        this.clearSpecies();
        this.resilience = resilience;
    }

    public void setFertilite(double fertilite) throws ConditionsInitialesInvalides {
        if (0 > fertilite || fertilite > 1) {
            throw new ConditionsInitialesInvalides("");
        }

        this.clearSpecies();
        this.fertilite = fertilite;
    }

    public void setAgeFertilite(int ageFertilite) throws ConditionsInitialesInvalides {
        if (ageFertilite < 0) {
            throw new ConditionsInitialesInvalides("");
        }

        this.clearSpecies();
        this.ageFertilite = ageFertilite;
    }

    public final void setEnergieEnfant(double energieEnfant) throws ConditionsInitialesInvalides {
        if (energieEnfant <= 0) {
            throw new ConditionsInitialesInvalides("");
        }

        this.clearSpecies();
        this.energieEnfant = energieEnfant;
    }

    public final void setTailleMaximum(double tailleMaximum) throws ConditionsInitialesInvalides {
        if (tailleMaximum <= 0) {
            throw new ConditionsInitialesInvalides("");
        }

        this.clearSpecies();
        this.tailleMaximum = tailleMaximum;
    }

    protected final String getNomEspece() throws ConditionsInitialesInvalides {
        if (this.nomEspece == null) {
            throw new ConditionsInitialesInvalides("nomEspece non spécifié");
        }

        return this.nomEspece;
    }

    protected final double getBesoinEnergie() throws ConditionsInitialesInvalides {
        if (this.besoinEnergie == null) {
            throw new ConditionsInitialesInvalides("");
        }

        return this.besoinEnergie;
    }

    protected final double getEfficaciteEnergie() throws ConditionsInitialesInvalides {
        if (this.efficaciteEnergie == null) {
            throw new ConditionsInitialesInvalides("");
        }

        return this.efficaciteEnergie;
    }

    protected final double getResilience() throws ConditionsInitialesInvalides {
        if (this.resilience == null) {
            throw new ConditionsInitialesInvalides("");
        }

        return this.resilience;
    }

    protected final double getFertilite() throws ConditionsInitialesInvalides {
        if (this.fertilite == null) {
            throw new ConditionsInitialesInvalides("");
        }

        return this.fertilite;
    }

    protected final int getAgeFertilite() throws ConditionsInitialesInvalides {
        if (this.ageFertilite == null) {
            throw new ConditionsInitialesInvalides("");
        }

        return this.ageFertilite;
    }

    protected final double getEnergieEnfant() throws ConditionsInitialesInvalides {
        if (this.energieEnfant == null) {
            throw new ConditionsInitialesInvalides("");
        }

        return this.energieEnfant;
    }

    protected final Sp getSpecies() throws ConditionsInitialesInvalides {
        if (this.species == null) {
            this.species = this.makeSpecies();
        }

        return this.species;
    }

    protected final double getTailleMaximum() throws ConditionsInitialesInvalides {
        if (this.tailleMaximum == null) {
            return 10 * this.getEnergieEnfant();
        }

        return this.tailleMaximum;
    }

    protected abstract Sp makeSpecies() throws ConditionsInitialesInvalides;
}
