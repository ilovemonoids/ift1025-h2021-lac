public final class UsinePlante extends UsineOrganisme<PlantSpecies> {
    public Plante creerPlante() throws ConditionsInitialesInvalides {
        return new Plante(
            this.getSpecies(),
            0,
            this.getEnergieEnfant());
    }

    @Override
    protected PlantSpecies makeSpecies() throws ConditionsInitialesInvalides {
        return new PlantSpecies(
            this.getNomEspece(),
            this.getBesoinEnergie(),
            this.getEfficaciteEnergie(),
            this.getResilience(),
            this.getFertilite(),
            this.getAgeFertilite(),
            this.getEnergieEnfant(),
            this.getTailleMaximum());
    }
}
