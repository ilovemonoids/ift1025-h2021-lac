public final class UsineHerbivore extends UsineAnimal<HerbivoreSpecies> {
    private Double voraciteMin;
    private Double voraciteMax;

    public void setVoraciteMin(Double voraciteMin) {
        this.clearSpecies();
        this.voraciteMin = voraciteMin;
    }

    public void setVoraciteMax(Double voraciteMax) {
        this.clearSpecies();
        this.voraciteMax = voraciteMax;
    }

    private double getVoraciteMin() {
        return this.voraciteMin;
    }

    private double getVoraciteMax() {
        return this.voraciteMax;
    }

    public Herbivore creerHerbivore() throws ConditionsInitialesInvalides {
        return new Herbivore(
            this.getSpecies(),
            0,
            this.getEnergieEnfant());
    }

    @Override
    protected HerbivoreSpecies makeSpecies() throws ConditionsInitialesInvalides {
        return new HerbivoreSpecies(
            this.getNomEspece(),
            this.getBesoinEnergie(),
            this.getEfficaciteEnergie(),
            this.getResilience(),
            this.getFertilite(),
            this.getAgeFertilite(),
            this.getEnergieEnfant(),
            this.getTailleMaximum(),
            this.getDebrouillardise(),
            this.getVoraciteMin(),
            this.getVoraciteMax(),
            this.getAliments());
    }
}
