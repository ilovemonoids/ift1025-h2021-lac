public final class UsineCarnivore extends UsineAnimal<CarnivoreSpecies> {
    public Carnivore creerCarnivore() throws ConditionsInitialesInvalides {
        return new Carnivore(
            this.getSpecies(),
            0,
            this.getEnergieEnfant());
    }

    @Override
    protected CarnivoreSpecies makeSpecies() throws ConditionsInitialesInvalides {
        return new CarnivoreSpecies(
            this.getNomEspece(),
            this.getBesoinEnergie(),
            this.getEfficaciteEnergie(),
            this.getResilience(),
            this.getFertilite(),
            this.getAgeFertilite(),
            this.getEnergieEnfant(),
            this.getTailleMaximum(),
            this.getDebrouillardise(),
            this.getAliments());
    }
}
