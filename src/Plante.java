public final class Plante extends Organism<PlantSpecies, Plante> {
    protected Plante(PlantSpecies species, int age, double energy) {
        super(species, age, energy);
    }

    public TickResult<Plante> tick(double energieDisponible) {
        return this.birthAndDeath(energieDisponible);
    }

    @Override
    protected Plante makeBaby() {
        return new Plante(this.getSpecies(), 0, this.getEnergieEnfant());
    }

    @Override
    protected Died<Plante> kill() {
        return new Died<>(this);
    }

    @Override
    public String toString() {
        return String.format("\"%s\"{energie=%.2f, age=%d}", this.getNomEspece(), this.getEnergie(), this.getAge());
    }
}
