import java.util.Collection;

public final class HerbivoreSpecies extends AnimalSpecies {
    private final double voraciteMin;
    private final double voraciteMax;

    public HerbivoreSpecies(
        String nomEspece,
        double besoinEnergie,
        double efficaciteEnergie,
        double resilience,
        double fertilite,
        int ageFertilite,
        double energieEnfant,
        double tailleMaximum,
        double debrouillardise,
        double voraciteMin,
        double voraciteMax,
        Collection<String> aliments)
    {
        super(
            nomEspece,
            besoinEnergie,
            efficaciteEnergie,
            resilience,
            fertilite,
            ageFertilite,
            energieEnfant,
            tailleMaximum,
            debrouillardise,
            aliments);
        this.voraciteMin = voraciteMin;
        this.voraciteMax = voraciteMax;
    }

    public double getVoraciteMin() {
        return this.voraciteMin;
    }

    public double getVoraciteMax() {
        return this.voraciteMax;
    }
}
