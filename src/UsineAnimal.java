import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class UsineAnimal<Sp> extends UsineOrganisme<Sp> {
    private Double debrouillardise;
    private final List<String> aliments = new ArrayList<>();

    public void setDebrouillardise(Double debrouillardise) {
        this.clearSpecies();
        this.debrouillardise = debrouillardise;
    }

    public void addAliment(String aliment) {
        this.clearSpecies();
        this.aliments.add(aliment);
    }

    protected double getDebrouillardise() {
        return this.debrouillardise;
    }

    protected Collection<String> getAliments() {
        return this.aliments;
    }
}
