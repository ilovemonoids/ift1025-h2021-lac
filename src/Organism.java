import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class Organism<Sp extends Species, This extends Organism<Sp, This>> {
    private final Sp species;
    private int age;
    private double energie;

    protected Organism(Sp species, int age, double energie) {
        this.species = species;
        this.age = age;
        this.energie = energie;
    }

    protected final Sp getSpecies() {
        return this.species;
    }

    public final String getNomEspece() {
        return this.species.getNomEspece();
    }

    public final double getBesoinEnergie() {
        return this.species.getBesoinEnergie();
    }

    public final double getEfficaciteEnergie() {
        return this.species.getEfficaciteEnergie();
    }

    public final double getResilience() {
        return this.species.getResilience();
    }

    public final double getFertilite() {
        return this.species.getFertilite();
    }

    public final int getAgeFertilite() {
        return this.species.getAgeFertilite();
    }

    public final double getEnergieEnfant() {
        return this.species.getEnergieEnfant();
    }

    public final double getTailleMaximum() {
        return this.species.getTailleMaximum();
    }

    public final int getAge() {
        return this.age;
    }

    public final double getEnergie() {
        return this.energie;
    }

    /**
     * Consomme une fraction k de l'organisme. Retourne la quantité d'énergie consommée.
     */
    public final double consume(double k) {
        var consumed = this.getEnergie() * k;
        this.energie -= consumed;
        return consumed;
    }

    protected abstract This makeBaby();

    protected abstract Died<This> kill();

    protected final TickResult<This> birthAndDeath(double energieDisponible) {
        this.age += 1;

        if (energieDisponible < this.getBesoinEnergie()) {
            var manque = this.getBesoinEnergie() - energieDisponible;
            if (this.getEnergie() < manque) {
                return this.kill();
            }

            var pSurvie = Math.pow(this.getResilience(), manque);
            if (Math.random() < pSurvie) {
                this.energie -= manque;
                return new NullResult<>();
            } else {
                return this.kill();
            }
        } else if (energieDisponible > this.getBesoinEnergie()) {
            var surplus = energieDisponible - this.getBesoinEnergie();
            TickResult<This> result = new NullResult<>();

            if (this.age >= this.getAgeFertilite()) {
                List<This> babies = new ArrayList<>();

                double roule = 0;
                while (surplus - roule > 0) {
                    if (Math.random() < this.getFertilite()) {
                        if (this.getEnergieEnfant() < surplus) {
                            surplus -= this.getEnergieEnfant();
                            roule = Math.max(roule - this.getEnergieEnfant(), 0);
                            babies.add(this.makeBaby());
                        } else {
                            if (this.getEnergieEnfant() < this.energie + surplus) {
                                this.energie -= this.getEnergieEnfant() - surplus;
                                babies.add(this.makeBaby());
                            }
                            break;
                        }
                    } else {
                        roule += 1;
                    }
                }


                result = new HadBabies<>(babies);
            }

            this.energie = Math.min(this.energie + surplus * this.getEfficaciteEnergie(), this.getTailleMaximum());
            return result;
        } else {
            return new NullResult<>();
        }
    }

    public interface TickResult<O> {
        void run(List<O> organisms);
    }

    private final static class NullResult<O> implements TickResult<O> {
        private NullResult() { }

        @Override
        public void run(List<O> organisms) {
        }
    }

    private final static class HadBabies<O> implements TickResult<O> {
        private final Collection<O> babies;

        private HadBabies(Collection<O> babies) {
            this.babies = babies;
        }

        @Override
        public void run(List<O> plantes) {
            plantes.addAll(this.babies);
        }
    }

    protected static final class Died<O> implements TickResult<O> {
        private final O organism;

        protected Died(O organism) {
            this.organism = organism;
        }

        @Override
        public void run(List<O> organisms) {
            organisms.remove(this.organism);
        }
    }
}
