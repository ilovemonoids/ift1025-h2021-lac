import java.util.Collection;

public class CarnivoreSpecies extends AnimalSpecies {
    public CarnivoreSpecies(
        String nomEspece,
        double besoinEnergie,
        double efficaciteEnergie,
        double resilience,
        double fertilite,
        int ageFertilite,
        double energieEnfant,
        double tailleMaximum,
        double debrouillardise,
        Collection<String> aliments)
    {
        super(
            nomEspece,
            besoinEnergie,
            efficaciteEnergie,
            resilience,
            fertilite,
            ageFertilite,
            energieEnfant,
            tailleMaximum,
            debrouillardise,
            aliments);
    }
}
