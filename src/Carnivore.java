import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static java.util.stream.Collectors.toUnmodifiableList;

public final class Carnivore extends Animal<CarnivoreSpecies, Carnivore> {
    public Carnivore(CarnivoreSpecies species, int age, double energie) {
        super(species, age, energie);
    }

    @Override
    protected Carnivore makeBaby() {
        return new Carnivore(this.getSpecies(), 0, this.getEnergieEnfant());
    }

    @Override
    protected Died<Carnivore> kill() {
        return new Died<>(this);
    }

    public Collection<HuntResult> hunt() {
        var preyCount = 0;
        while (Math.random() < this.getDebrouillardise()) {
            preyCount += 1;
        }

        if (preyCount > 0) {
            return Collections.singleton(this.new HuntResult(preyCount));
        } else {
            return Collections.emptyList();
        }
    }

    public final class HuntResult {
        private final int preyCount;

        public HuntResult(int preyCount) {
            this.preyCount = preyCount;
        }

        public void eat(List<Herbivore> herbivores) {
            var candidates = herbivores.stream()
                .filter(herbivore -> Carnivore.this.getAliments().contains(herbivore.getNomEspece()))
                .filter(herbivore -> herbivore.getEnergie() < Carnivore.this.getEnergie())
                .collect(toUnmodifiableList());

            for (int i = 0; i < this.preyCount && !candidates.isEmpty(); ++i) {
                var eaten = candidates.get(ThreadLocalRandom.current().nextInt(candidates.size()));
                herbivores.remove(eaten);
                Carnivore.this.consumeEnergy(eaten.getEnergie());
            }
        }
    }
}
